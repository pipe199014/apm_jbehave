package com.bancolombia.APM.util;

import org.openqa.selenium.Alert;

import net.serenitybdd.core.pages.PageObject;

/**
 * Esta clase define métodos que van hacer reutilizados continuamente
 * @author Steven Trujillo
 *
 */
public class Utilities extends PageObject{

	private boolean accepNextAlert= true;
	
	/*
	 * método para el manejo de tiempos de carga de un elemento a otro
	 */
	public void tiempoMuerto(int segundos) {
		try {
			Thread.sleep(segundos);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * método para el manejo de alerts
	 */
	public String closeAlertAndGetItsText() {
	    try {
	      Alert alert = this.getDriver().switchTo().alert();
	      String alertText = alert.getText();
	      if (accepNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	    	accepNextAlert = true;
	    }
	  }
}

