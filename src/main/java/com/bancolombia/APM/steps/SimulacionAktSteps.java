package com.bancolombia.APM.steps;

import org.fluentlenium.core.annotation.Page;


import com.bancolombia.APM.pages.SimulacionPages;

import net.thucydides.core.annotations.Step;

/**
 * Esta clase define los pasos que debe realizar el usuario para simular un credito con la alianza AKT
 * @author: Steven Trujillo 
 */

public class SimulacionAktSteps {

	@Page
	SimulacionPages simulacionAKT;

	@Step
	public void ingresarVendedorAKT(String PuntoVenta ) {
		simulacionAKT.infoVendedor(PuntoVenta);
			
	}

	@Step
	public void ingresarProductoAKT(String Categoria,String SubCategoria,String ValorProd) {
		simulacionAKT.infoProducto(Categoria, SubCategoria, ValorProd); 
		
	}
	
	@Step
	public void ingresarProducMoto(String Marca,String Linea) {
		simulacionAKT.infoProducto_Moto_Carro(Marca, Linea);
	}
	@Step
	public void adicionarAccesorios(String Categoria_Acces_Tram, String SubCategoria_Acces_Tram,String valor_Acces_Tram) {
		simulacionAKT.adicionarAccesoriosTramites(Categoria_Acces_Tram, SubCategoria_Acces_Tram, valor_Acces_Tram);
	}
	
	@Step
	public void adicionarTramiteAccesori(String Categoria_Acces_Tram_2, String SubCategoria_Acces_Tram_2, String valor_Acces_Tram_2) {
		simulacionAKT.adicionarTramitesAccesorios_Add(Categoria_Acces_Tram_2, SubCategoria_Acces_Tram_2, valor_Acces_Tram_2);
	}
	
	@Step
	public void ingresarSolicitudAKT(String Tipo_Doc,String Num_Doc,String Nombre_1 ,String Nombre_2 , String Apellido_1, String Apellido_2, String Celular, String Correo )   {
		simulacionAKT.infoSolicitud(Tipo_Doc, Num_Doc, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Celular, Correo);
		
	}

	@Step
	public void simularCreditoAkt() {
		simulacionAKT.simular();
	}
	
	public void informacionCreditoVerificadaAKT() {
		simulacionAKT.VerificarInformacionCredito();
	}

	public void radicarSimulacionAKT() {
		simulacionAKT.Radicar();
	}

	public void aceptarCondicionSimularAKT() {
		simulacionAKT.acepetarCondicionesSimulado();
	}
	
	public void agregarTramiteAccesorio() {
		simulacionAKT.AgregaTramiteAccesorio();
	}
	 
}

