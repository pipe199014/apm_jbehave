package com.bancolombia.APM.steps;

import org.fluentlenium.core.annotation.Page;

import com.bancolombia.APM.pages.SimulacionPages;

import net.thucydides.core.annotations.Step;

/**
 * Esta clase define los pasos que debe realizar el usuario para simular un credito con la alianza SUFI
 * @author: Steven Trujillo 
 */
public class SimulacionSUFISteps {
	
	@Page
	SimulacionPages simulacionSUFI;
	
	@Step
	public void ingresarVendedorSUFI(String PuntoVenta ) {
		simulacionSUFI.infoVendedor(PuntoVenta);
	}

	@Step
	public void ingresarProductoSUFI(String Categoria,String SubCategoria,String ValorProd) {
		simulacionSUFI.infoProducto(Categoria, SubCategoria, ValorProd);		
	}
	
	@Step
	public void ingresarProductoCarro(String Marca,String Linea) {
		simulacionSUFI.infoProducto_Moto_Carro(Marca, Linea);
	}
	@Step
	public void adicionarAccesoriosTramites(String Categoria_Acces_Tram, String SubCategoria_Acces_Tram,String valor_Acces_Tram) {
		simulacionSUFI.adicionarAccesoriosTramites(Categoria_Acces_Tram, SubCategoria_Acces_Tram, valor_Acces_Tram);
	}
	
	@Step
	public void adicionarAccesorioTramite_Add(String Categoria_Acces_Tram_2, String SubCategoria_Acces_Tram_2, String valor_Acces_Tram_2) {
		simulacionSUFI.adicionarTramitesAccesorios_Add(Categoria_Acces_Tram_2, SubCategoria_Acces_Tram_2, valor_Acces_Tram_2);
	}
	
	public void agregarTramiteAccesorio() {
		simulacionSUFI.AgregaTramiteAccesorio();
	}

	@Step
	public void ingresarSolicitudSUFI(String Tipo_Doc,String Num_Doc,String Nombre_1 ,String Nombre_2 , String Apellido_1, String Apellido_2, String Celular, String Correo )   {
		simulacionSUFI.infoSolicitud(Tipo_Doc, Num_Doc, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Celular, Correo);
	}
	
	@Step
	public void tieneDeudor() {
		simulacionSUFI.deudor();
	}
	
	@Step
	public void ingresarDeudor(String Tipo_Doc_Estudiante, String Num_Doc_Estudiante, String Nombre_1_Estudiante, String Nombre_2_Estudiante, String Apellido_1_Estudiante, String Apellido_2_Estudiante 
			,String Celular_Estudiante ,String Correo_Estudiante, String Tipo_Doc,String Num_Doc,String Nombre_1 ,String Nombre_2 , String Apellido_1, String Apellido_2, String Celular, String Correo, String estudianteEsDeudor ) {
		simulacionSUFI.infoEstudiante(Tipo_Doc_Estudiante, Num_Doc_Estudiante, Nombre_1_Estudiante, Nombre_2_Estudiante, Apellido_1_Estudiante, Apellido_2_Estudiante, Celular_Estudiante, Correo_Estudiante,Tipo_Doc, Num_Doc, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Celular, Correo , estudianteEsDeudor);
	}
	
	@Step
	public void simularCreditoSUFI() {
		simulacionSUFI.simular();
	}
	
	@Step
	public void informacionCreditoVerificadaSUFI() {
		simulacionSUFI.VerificarInformacionCredito();
	}
	
	@Step
	public void radicarSimulacionSUFI() {
		simulacionSUFI.Radicar();
	}
	
	@Step
	public void aceptarCondicionSimularSUFI() {
		simulacionSUFI.acepetarCondicionesSimulado();
	}
	
}
