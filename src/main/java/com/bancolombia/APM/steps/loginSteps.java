package com.bancolombia.APM.steps;

import org.fluentlenium.core.annotation.Page;

import com.bancolombia.APM.pages.loginPages;

import net.thucydides.core.annotations.Step;

/**
 * Esta clase define los pasos que debe realizar el usuario para logearse en la aplicación de sufi
 * @author: Steven Trujillo 
 */
public class loginSteps {

	@Page
	loginPages login;

	@Step
	public void abrirApp() {
		login.open(); 
	}

	@Step
	public void inicioSesion(String Usuario, String Password) {
		 login.login(Usuario,Password);		
	}

	@Step
	public void validarIngresoASimular() {
		 login.validarSimular();
		
	}
	 
}


