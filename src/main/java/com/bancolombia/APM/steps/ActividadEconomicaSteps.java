 


	package com.bancolombia.APM.steps;

	import org.fluentlenium.core.annotation.Page;

	import com.bancolombia.APM.pages.actividadEconomicaPage;

	import net.thucydides.core.annotations.Step;

	/**
	 *  
	 */
  public class ActividadEconomicaSteps {
		@Page
		actividadEconomicaPage ActividadEconocmica;
		 
		@Step
		public void validarPagina() {
			ActividadEconocmica.validarPagina(); 
		}

		@Step
		public void seleccionarActividadEconomica(String ocupacion) {
			ActividadEconocmica.seleccionarActividadEconomica(ocupacion );		
		}

	 
	}

