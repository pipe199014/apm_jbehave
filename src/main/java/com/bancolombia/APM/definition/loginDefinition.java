package com.bancolombia.APM.definition;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.bancolombia.APM.steps.loginSteps;

import net.thucydides.core.annotations.Steps;

/**
 * Esta clase define el flujo y historia de usuario para logearse en la aplicación de sufi
 * @author: Steven Trujillo 
 */

public class loginDefinition {

	@Steps
	loginSteps logeo;
	
	@Given("que estoy en la aplicación de sufi")
	public void abrirUrl() {
		logeo.abrirApp();
	
	}

	@When("inicio sesion con usuario y contraseña")
	public void iniciarSesion(@Named("Usuario") String Usuario,@Named("Password")String password) {
		logeo.inicioSesion(Usuario,password);
	}
	
	@Then("verifico login correcto y doy clic en simular")
	public void validarAcceso() {
		logeo.validarIngresoASimular();;
	}

	@Given("Hacer Login")
	public void loginCompleto(@Named("Usuario") String Usuario,@Named("Password")String password) {
		logeo.abrirApp();
		logeo.inicioSesion(Usuario,password);
		logeo.validarIngresoASimular();;
	}
}


