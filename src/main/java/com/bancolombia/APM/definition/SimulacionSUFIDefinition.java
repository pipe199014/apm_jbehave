package com.bancolombia.APM.definition;

import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.bancolombia.APM.steps.SimulacionSUFISteps;

import net.thucydides.core.annotations.Steps;

/**
 * Esta clase define el flujo y historia de usuario para realizar credito con la alianza SUFI
 * @author: Steven Trujillo 
 */
public class SimulacionSUFIDefinition {
	
	@Steps
	SimulacionSUFISteps simularSUFI;
	
	@When ("diligencio la informacion vendedor")
	public void InformacionVendedorSUFI(@Named("PuntoVenta")String PuntoVenta) {
		simularSUFI.ingresarVendedorSUFI(PuntoVenta);
	}
	
	@When ("diligencio la informacion producto SUFI")
	public void InformacionProductoSUFI(@Named("Categoria")String Categoria,@Named("SubCategoria")String SubCategoria,@Named("ValorProd")String ValorProd) {
		simularSUFI.ingresarProductoSUFI(Categoria, SubCategoria, ValorProd);
	}
	
	@When ("diligencio la informacion de los campos Marca y Linea")
	public void InformacionProductoCarro(@Named("Marca")String Marca,@Named("Linea")String Linea) {
		simularSUFI.ingresarProductoCarro(Marca, Linea);
	}
	
	@Then ("doy clic en agregar tramites y accesorios")
	public void agregarTramitesAccesorios() {
		simularSUFI.agregarTramiteAccesorio();
	}
	
	@When ("diligencio los campos para registrar accesorios o tramites")
	public void adicionaraccesorios(@Named("Categoria_Acces_Tram")String Categoria_Acces_Tram, @Named("SubCategoria_Acces_Tram")String SubCategoria_Acces_Tram, @Named("valor_Acces_Tram")String valor_Acces_Tram){
		simularSUFI.adicionarAccesoriosTramites(Categoria_Acces_Tram, SubCategoria_Acces_Tram, valor_Acces_Tram);
	}
	
	@When ("agrego otro tramite o accesorio")
	public void adicionarTramitesAccesorio(@Named("Categoria_Acces_Tram_2")String Categoria_Acces_Tram_2, @Named("SubCategoria_Acces_Tram_2")String SubCategoria_Acces_Tram_2, @Named("valor_Acces_Tram_2")String valor_Acces_Tram_2){
		simularSUFI.adicionarAccesorioTramite_Add(Categoria_Acces_Tram_2, SubCategoria_Acces_Tram_2, valor_Acces_Tram_2);
	}
	
	@When ("diligencio la informacion solicitante")
	public void InformacionSolicitanteSUFI(@Named("Tipo_Doc")String Tipo_Doc,@Named("Num_Doc")String Num_Doc,@Named("Nombre_1")String Nombre_1,@Named("Nombre_2")String Nombre_2,@Named("Apellido_1")String Apellido_1,@Named("Apellido_2")String Apellido_2 
	,@Named("Celular")String Celular ,@Named("Correo")String Correo)   {
		simularSUFI.ingresarSolicitudSUFI(Tipo_Doc, Num_Doc, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Celular, Correo);
	}
	
	/*@When ("el deudor es diferente al estudiante")
	public void deudorEstudiante() {
		simularSUFI.tieneDeudor();
	}*/
	
	@When ("diligencio la informacion solicitante y-o Estudiante")
	public void deudorEstudiante (@Named("Tipo_Doc_Estudiante")String Tipo_Doc_Estudiante,@Named("Num_Doc_Estudiante")String Num_Doc_Estudiante,@Named("Nombre_1_Estudiante")String Nombre_1_Estudiante,@Named("Nombre_2_Estudiante")String Nombre_2_Estudiante,@Named("Apellido_1_Estudiante")String Apellido_1_Estudiante,@Named("Apellido_2_Estudiante")String Apellido_2_Estudiante 
			,@Named("Celular_Estudiante")String Celular_Estudiante ,@Named("Correo_Estudiante")String Correo_Estudiante,@Named("Tipo_Doc")String Tipo_Doc,@Named("Num_Doc")String Num_Doc,@Named("Nombre_1")String Nombre_1,@Named("Nombre_2")String Nombre_2,@Named("Apellido_1")String Apellido_1,@Named("Apellido_2")String Apellido_2 
			,@Named("Celular")String Celular ,@Named("Correo")String Correo, @Named("estudianteEsDeudor")String estudianteEsDeudor) {
		simularSUFI.ingresarDeudor(Tipo_Doc_Estudiante, Num_Doc_Estudiante, Nombre_1_Estudiante, Nombre_2_Estudiante, Apellido_1_Estudiante, Apellido_2_Estudiante, Celular_Estudiante, Correo_Estudiante,Tipo_Doc, Num_Doc, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Celular, Correo, estudianteEsDeudor);
	}
	
	@Then ("doy click en el boton de simular")
	public void simularCreditoSUFI(){
		simularSUFI.aceptarCondicionSimularSUFI();
	}
	
	@Then ("verifico la informacion del credito solicitado")
	public void verificarInfoCreditoSUFI() {
		simularSUFI.informacionCreditoVerificadaSUFI();
	}
	
	@When ("doy click en el boton radicar")
	public void radicarCreditoSUFI() {
		simularSUFI.radicarSimulacionSUFI();
	}
	
	@When("acepto las condiciones de la simulacion")
	public void aceptarCondicionesCreditoSUFI() {
		simularSUFI.aceptarCondicionSimularSUFI();
	}
			
}


