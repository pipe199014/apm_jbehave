package com.bancolombia.APM.definition;
 
	import org.jbehave.core.annotations.Given;
	import org.jbehave.core.annotations.Named;
	import org.jbehave.core.annotations.Then;
	import org.jbehave.core.annotations.When;

	import com.bancolombia.APM.steps.ActividadEconomicaSteps;

	import net.thucydides.core.annotations.Steps;

	/**
	 * Esta clase define el flujo y historia de usuario para ingresar y seleccionar una
	 *  Actividad Economica 
 
	 */
 
		public class ActividadEconomicaDefinition {

		@Steps
		ActividadEconomicaSteps ActividadEconomica;
		
		@When("Valido que me encuentre el la pagina de actividad economica")
		public void validarPagina() {
			ActividadEconomica.validarPagina();
		
		}

		@Then("selecciono la actividad economica y doy clik en continual")
		public void SeleccionarActividadEconomica(@Named("ocupacion") String ocupacion ) {
			ActividadEconomica.seleccionarActividadEconomica(ocupacion);
		}
	 
	}