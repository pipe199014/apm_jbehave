package com.bancolombia.APM.definition;

import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.bancolombia.APM.steps.SimulacionAktSteps;

import com.bancolombia.APM.steps.loginSteps;

import net.thucydides.core.annotations.Steps;

/**
 * Esta clase define el flujo y historia de usuario para realizar credito con la alianza AKT
 * @author: Steven Trujillo 
 */
public class SimulacionAKTDefinition {
	
	@Steps
	SimulacionAktSteps simularAKT;
	
	
	@When ("diligencio la informacion vendedor")
	public void InformacionVendedorAKT(@Named("PuntoVenta")String PuntoVenta) {
		simularAKT.ingresarVendedorAKT(PuntoVenta);
		
	}
	
	@When ("diligencio la informacion producto AKT")
	public void InformacionProductoAKT(@Named("Categoria")String Categoria,@Named("SubCategoria")String SubCategoria,@Named("ValorProd")String ValorProd) {
		simularAKT.ingresarProductoAKT(Categoria, SubCategoria, ValorProd);
	}
	
	@When ("diligencio la informacion de los campos Marca y Linea")
	public void InformacionProductoMoto(@Named("Marca")String Marca,@Named("Linea")String Linea) {
		simularAKT.ingresarProducMoto(Marca, Linea);
	}
	
	@Then ("doy clic en agregar tramites y accesorios")
	public void agregarTramitesAccesorios() {
		simularAKT.agregarTramiteAccesorio();
	}
	
	@When ("diligencio los campos para registrar accesorios o tramites")
	public void adicionarAccesorio(@Named("Categoria_Acces_Tram")String Categoria_Acces_Tram, @Named("SubCategoria_Acces_Tram")String SubCategoria_Acces_Tram, @Named("valor_Acces_Tram")String valor_Acces_Tram){
		simularAKT.adicionarAccesorios(Categoria_Acces_Tram, SubCategoria_Acces_Tram, valor_Acces_Tram);
	}
	
	@When ("agrego otro tramite o accesorio")
	public void adicionarTramitesAccesorio(@Named("Categoria_Acces_Tram_2")String Categoria_Acces_Tram_2, @Named("SubCategoria_Acces_Tram_2")String SubCategoria_Acces_Tram_2, @Named("valor_Acces_Tram_2")String valor_Acces_Tram_2){
		simularAKT.adicionarTramiteAccesori(Categoria_Acces_Tram_2, SubCategoria_Acces_Tram_2, valor_Acces_Tram_2);
		
	}
		
	@When ("diligencio la informacion solicitante")
	public void InformacionSolicitanteAKT(@Named("Tipo_Doc")String Tipo_Doc,@Named("Num_Doc")String Num_Doc,@Named("Nombre_1")String Nombre_1,@Named("Nombre_2")String Nombre_2,@Named("Apellido_1")String Apellido_1,@Named("Apellido_2")String Apellido_2 
	,@Named("Celular")String Celular ,@Named("Correo")String Correo)   {
		simularAKT.ingresarSolicitudAKT(Tipo_Doc, Num_Doc, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Celular, Correo);
	}
	
	@Then ("doy click en el boton de simular")
	public void simularCreditoAKT(){
		simularAKT.simularCreditoAkt();
	}
	
	@When ("verifico la informacion del credito solicitado")
	public void verificarInfoCreditoAKT() {
		simularAKT.informacionCreditoVerificadaAKT();
	}

	@Then ("doy click en el boton radicar")
	public void radicarCreditoAKT() {
		simularAKT.radicarSimulacionAKT();
	}

	@When("acepto las condiciones de la simulacion")
	public void aceptarCondicionesCreditoAKT() {
		simularAKT.aceptarCondicionSimularAKT();
	}
	
	@Given("Simulacion AKT")
	public void SimulacionCompletaAKT(
			@Named("PuntoVenta")String PuntoVenta,
			@Named("Categoria")String Categoria,
			@Named("SubCategoria")String SubCategoria,
			@Named("ValorProd")String ValorProd,
			@Named("Marca")String Marca,
			@Named("Linea")String Linea,
			@Named("Tipo_Doc")String Tipo_Doc,
			@Named("Num_Doc")String Num_Doc,
			@Named("Nombre_1")String Nombre_1,
			@Named("Nombre_2")String Nombre_2,
			@Named("Apellido_1")String Apellido_1,
			@Named("Apellido_2")String Apellido_2,
			@Named("Celular")String Celular ,
			@Named("Correo")String Correo) {
		simularAKT.ingresarVendedorAKT(PuntoVenta);
		simularAKT.ingresarProductoAKT(Categoria, SubCategoria, ValorProd);
		simularAKT.ingresarProducMoto(Marca, Linea);	
		simularAKT.ingresarSolicitudAKT(Tipo_Doc, Num_Doc, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Celular, Correo);
		simularAKT.simularCreditoAkt();
		simularAKT.informacionCreditoVerificadaAKT();
		simularAKT.radicarSimulacionAKT();
		simularAKT.aceptarCondicionSimularAKT();
	}
}

