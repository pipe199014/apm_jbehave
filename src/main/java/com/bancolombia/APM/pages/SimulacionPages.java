package com.bancolombia.APM.pages;

import static org.junit.Assert.assertEquals;

import org.hamcrest.MatcherAssert; 
import org.openqa.selenium.support.FindBy;

import com.bancolombia.APM.util.Utilities;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

/**
 * Esta clase contiene los elementos de la página simulación de credito
 * @author: Steven Trujillo 
 */

public class SimulacionPages extends PageObject{

	Utilities util= new Utilities();
	
	@FindBy(xpath = "//*[contains(@id, 'wtApplication_PuntoDeVenta')]")
	private WebElementFacade cbxPuntoVenta;

	@FindBy(xpath = "//*[ contains(@id, 'wtProduct_Categoria') and contains (@onkeyup, 'this.onchange()')]")
	private WebElementFacade cbxCategoria;

	@FindBy(id = "om_apm_wt44:wtMainContent:wtProduct_Subcategoria")
	private WebElementFacade cbxSubCategoria;

	@FindBy(xpath = "//*[contains(@id, 'wtProduct_Marca') ]")
	private WebElementFacade cbxMarca;

	@FindBy(id = "om_apm_wt44:wtMainContent:wtProduct_Linea")
	private WebElementFacade cbxLinea;

	@FindBy(xpath = "//*[contains(@id, 'wtProduct_ValorProducto')]")
	private WebElementFacade txtValorProducto;

	@FindBy(id = "om_apm_wt44:wtMainContent:wtBasePersonApplicant_IdentificationTypeId")
	private WebElementFacade cbxTipoDoc;

	@FindBy(xpath = "//*[contains(@id, 'wtBasePersonApplicant_IdentificationValue')]")
	private WebElementFacade txtNumDocumento;

	@FindBy(xpath = "//*[contains(@id, 'wtBasePersonApplicant_FirstName')]")
	private WebElementFacade txtNombre1;

	@FindBy(xpath = "//*[contains(@id, 'wtBasePersonApplicant_SecondName')]")
	private WebElementFacade txtNombre2;

	@FindBy(xpath = "//*[contains(@id, 'wtBasePersonApplicant_FirstLastName')]")
	private WebElementFacade txtApellido1;

	@FindBy(xpath = "//*[contains(@id, 'wtBasePersonApplicant_SecondLastName')]")
	private WebElementFacade txtApellido2;

	@FindBy(xpath = "//*[contains(@id, 'wtApplicant_BaseContactPhone_PhoneNumber')]")
	private WebElementFacade txtCelular;

	@FindBy(xpath = "//*[contains(@id, 'wtBasePersonApplicant_BaseContactEmail_EmailAddress')]")
	private WebElementFacade txtCorreo;

	@FindBy(xpath = "//*[contains(@id, 'wtSimuladorValorTotal')]")
	private WebElementFacade SimuladorValorTotal;

	@FindBy(xpath = "//*[contains(@id, 'wtSimularButton')]")
	private WebElementFacade btnSimular;
	
	@FindBy(xpath = "//*[contains(@id, 'wtRadicarButton')]")
	private WebElementFacade btnRadicar;

	@FindBy(xpath = "//*[contains(@id, 'wtApplicant_IsEstudiante_Si')]")
	private WebElementFacade rdDeudorSi;
	
	@FindBy(xpath = "//*[contains(@id, 'wtApplicant_IsEstudiante_No')]")
	private WebElementFacade rdDeudorNo;
	
	@FindBy(xpath = "//*[contains(@id, 'wtBasePersonApplicant_BaseContactEmail_EmailAddress')]")
	private WebElementFacade txtCorreoDeudor;

	@FindBy(xpath = "//*[contains(@id, 'wtEstudiante_TipoDocumento')]")
	private WebElementFacade cbxTipoDocEstudiante;

	@FindBy(xpath = "//*[contains(@id, 'wtEstudiante_NumeroDocumento')]")
	private WebElementFacade txtNumDocumentoEstudiante;

	@FindBy(xpath = "//*[contains(@id, 'wtEstudiante_PrimeiroNombre')]")
	private WebElementFacade txtNombre1Estudiante;

	@FindBy(xpath = "//*[contains(@id, 'wtEstudiante_SegundoNombre')]")
	private WebElementFacade txtNombre2Estudiante;

	@FindBy(xpath = "//*[contains(@id, 'wtEstudiante_PrimeiroApellido')]")
	private WebElementFacade txtApellido1Estudiante;

	@FindBy(xpath = "//*[contains(@id, 'wtEstudiante_SegundoApellido')]")
	private WebElementFacade txtApellido2Estudiante;

	@FindBy(xpath = "//*[contains(@id, 'wtEstudiante_Celular')]")
	private WebElementFacade txtCelularEstudiante;

	@FindBy(xpath = "//*[contains(@id, 'wtEstudiante_CorreoElectronico')]")
	private WebElementFacade txtCorreoEstudiante;
 
	//tramites y accesorios
		
		@FindBy(xpath = "//*[contains(@id, 'wtAggregarAccesorioButton')]")
		private WebElementFacade btnAgregarTramiteAccesorio; 
		
		@FindBy(xpath = "//*[contains(@id, 'wtTramiteAccesorio_LK_CategoriaId')]")
		private WebElementFacade cbxCategoriaTA;
		
		@FindBy(xpath = "//*[contains(@id, 'wtTramiteAccesorio_LK_CategoriaId2')]")
		private WebElementFacade cbxCategoriaTA2; 
		
		@FindBy(id="om_apm_wt44:wtMainContent:wtTramiteAccesorio_LK_SubCategoriaId")
		private WebElementFacade cbxSubCategoriaTA;
		
		@FindBy(id="om_apm_wt44:wtMainContent:wtTramiteAccesorio_LK_SubCategoriaId2")
		private WebElementFacade cbxSubCategoriaTA2;
		
		@FindBy(xpath="//*[contains(@id, 'wtTramiteAccesorio_Valor')]") 
		private WebElementFacade txtValorAcces; 
		
		@FindBy(xpath = "//*[contains(@id, 'wtTramiteAccesorio_Valor2')]")
		private WebElementFacade txtValorTram; 

	public void infoVendedor(String PuntoVenta) {
		cbxPuntoVenta.selectByValue(PuntoVenta);
		
	}

	public void infoProducto(String Categoria, String SubCategoria, String ValorProd) {
		cbxCategoria.selectByValue(Categoria);
		util.tiempoMuerto(2000);
		cbxSubCategoria.selectByValue(SubCategoria); 
		txtValorProducto.sendKeys(ValorProd);
		
	}
	
	public void AgregaTramiteAccesorio() {
		util.tiempoMuerto(2000);
		btnAgregarTramiteAccesorio.click();
		util.tiempoMuerto(2000);
	}
	
	public void infoProducto_Moto_Carro(String Marca, String Linea) {
		cbxMarca.selectByValue(Marca);
		util.tiempoMuerto(3000);
		cbxLinea.selectByValue(Linea);
	}
	
	public void adicionarAccesoriosTramites(String Categoria_Acces_Tram, String SubCategoria_Acces_Tram,String valor_Acces_Tram) {
		
		util.tiempoMuerto(2000);
		cbxCategoriaTA.selectByValue(Categoria_Acces_Tram); 
		util.tiempoMuerto(3000);
		cbxSubCategoriaTA.selectByValue(SubCategoria_Acces_Tram);
		txtValorAcces.sendKeys(valor_Acces_Tram);;
	}
	
	public void adicionarTramitesAccesorios_Add(String Categoria_Acces_Tram_2, String SubCategoria_Acces_Tram_2, String valor_Acces_Tram_2){		
		AgregaTramiteAccesorio();
		cbxCategoriaTA2.selectByValue(Categoria_Acces_Tram_2);
		util.tiempoMuerto(3000);
		cbxSubCategoriaTA2.selectByValue(SubCategoria_Acces_Tram_2);
		txtValorTram.sendKeys(valor_Acces_Tram_2);
		 
	}

	public void infoSolicitud(String Tipo_Doc, String Num_Doc, String Nombre_1, String Nombre_2, String Apellido_1,
			String Apellido_2, String Celular, String Correo) {
		util.tiempoMuerto(3000);
		cbxTipoDoc.selectByValue(Tipo_Doc);
		txtNumDocumento.sendKeys(Num_Doc);
		txtNombre1.sendKeys(Nombre_1);
		txtNombre2.sendKeys(Nombre_2);
		txtApellido1.sendKeys(Apellido_1);
		txtApellido2.sendKeys(Apellido_2);
		txtCelular.sendKeys(Celular);
		txtCorreo.sendKeys(Correo);
		util.tiempoMuerto(1000);
	}
	
	public void deudor()
	{
		
		rdDeudorNo.click();
		  
	}
	
	public void infoEstudiante (String Tipo_Doc_Estudiante, String Num_Doc_Estudiante, String Nombre_1_Estudiante, String Nombre_2_Estudiante, String Apellido_1_Estudiante, String Apellido_2_Estudiante 
			,String Celular_Estudiante ,String Correo_Estudiante,String Tipo_Doc, String Num_Doc, String Nombre_1, String Nombre_2, String Apellido_1,
			String Apellido_2, String Celular, String Correo, String estudianteEsDeudor) {
		 
		if (estudianteEsDeudor.equals("No")) {
			util.tiempoMuerto(3000);
			rdDeudorNo.click();
			util.tiempoMuerto(3000);
			infoSolicitud(Tipo_Doc, Num_Doc, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Celular, Correo);
			util.tiempoMuerto(3000);
			cbxTipoDocEstudiante.selectByValue(Tipo_Doc_Estudiante);
			txtNumDocumentoEstudiante.sendKeys(Num_Doc_Estudiante);
			txtNombre1Estudiante.sendKeys(Nombre_1_Estudiante);
			txtNombre2Estudiante.sendKeys(Nombre_2_Estudiante);
			txtApellido1Estudiante.sendKeys(Apellido_1_Estudiante);
			txtApellido2Estudiante.sendKeys(Apellido_2_Estudiante);
			txtCelularEstudiante.sendKeys(Celular_Estudiante);
			txtCorreoEstudiante.sendKeys(Correo_Estudiante);
			util.tiempoMuerto(2000);	
		}
		else {
			rdDeudorSi.click();
			infoSolicitud(Tipo_Doc, Num_Doc, Nombre_1, Nombre_2, Apellido_1, Apellido_2, Celular, Correo); 
		}
		
		
	}

	public void simular() {
		btnSimular.click();
		util.tiempoMuerto(5000);//se ponen 5 segundos en cuanto se valide el tiempo que debe demorar en responder
	}
	
	public void VerificarInformacionCredito() {
		util.tiempoMuerto(2000);
		verificarValorProducto();
	}
	
	public void Radicar() {
		btnRadicar.click();
		util.tiempoMuerto(3000);
	}
	
	public void acepetarCondicionesSimulado() {
		assertEquals(util.closeAlertAndGetItsText(), "Tu solicitud será radicada con las condiciones seleccionadas.");
		util.tiempoMuerto(2000);
	}
	
	public void verificarValorProducto() {
		String valorSimulacionCadena = SimuladorValorTotal.getText();
		String repla = valorSimulacionCadena.replace(".", "");
		int simular = Integer.parseInt(repla);
		MatcherAssert.assertThat("valor nulo", simular > 0);
	}
}

