	package com.bancolombia.APM.pages; 
	import org.openqa.selenium.support.FindBy; 
	import com.bancolombia.APM.util.Utilities; 
	import net.serenitybdd.core.pages.PageObject;
	import net.serenitybdd.core.pages.WebElementFacade; 

	/**
	 * Esta clase contiene los elementos de la página Actividad Economica 
	 *  
	 */
 
	public class actividadEconomicaPage  extends PageObject {
		
		Utilities util= new Utilities();

		@FindBy(xpath = "//*[contains(@id, 'wtOcupationInput')]")
		private WebElementFacade cbxOcupation;
		
		//unicamente para independiente profecional
		@FindBy(xpath = "//*[contains(@id, 'wtProfesionFuente')]")
		private WebElementFacade cbxProfesion;
		
		/*122 --129
		 * tiene rut independiente e independiente profecional*/
		@FindBy(xpath = "//*[contains(@id, 'wt58')]")
		private WebElementFacade rdTieneRut ;
		
		@FindBy(xpath = "//*[contains(@id, 'wt97')]")
		private WebElementFacade rdNoTieneRud;
		
		//tiene establecimiento propio independiente e independiente profecional
		@FindBy(xpath = "//*[contains(@id, 'wtPlaceholder1:wt19')]")
		private WebElementFacade rdTieneEmpresa ;
		
		@FindBy(xpath = "//*[contains(@id, 'wt80')]")
		private WebElementFacade rdNoTieneEmpresa ;
		
		//solo para independiente  descripcion de la actividad
		@FindBy(xpath = "//*[contains(@id, 'wtAutocompleteLK_CIIU')]")
		private WebElementFacade  txtDescripcionActividad ;
		
		@FindBy(xpath = "//*[contains(@id, 'wt81')]")
		private WebElementFacade btnradicar ;
		 
		@FindBy(xpath = "//*[contains(@id, 'wt81')]")
		private WebElementFacade btnOcupation;
 
		
		public void validarPagina( ) {
			 
		} 
		
		public void seleccionarActividadEconomica(String ocupacion) {
			cbxOcupation.selectByValue(ocupacion);  
			util.tiempoMuerto(3000);
			rdTieneRut.click();
			util.tiempoMuerto(3000);
			rdTieneEmpresa.click();
			txtDescripcionActividad.sendKeys("1072-Elaboración de panela");
			util.tiempoMuerto(3000); 
			btnradicar.click();
			util.tiempoMuerto(3000);
		} 
		
		
 

	}
