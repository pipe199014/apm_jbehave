package com.bancolombia.APM.pages;

import org.hamcrest.MatcherAssert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import com.bancolombia.APM.util.Utilities;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

/**
 * Esta clase contiene los elementos de la página login 
 * @author: Steven Trujillo 
 */

@DefaultUrl("http://10.8.69.166/om_apm_common/Login.jsf")

public class loginPages extends PageObject {
	
	Utilities util= new Utilities();

	@FindBy(xpath = "//*[contains(@id, 'wtUserNameInput')]")
	private WebElementFacade txtUsuario;

	@FindBy(xpath = "//*[contains(@id, 'wtPasswordInput')]")
	private WebElementFacade txtPassword;

	@FindBy(xpath = "//*[contains(@id, 'wt34')]") 
	private WebElementFacade btnIngreso;

	@FindBy(xpath = "//*[contains(@href, '/om_apm_consumer/Application_Edit.jsf')]") 
	private WebElementFacade simular;
 
	public loginPages(WebDriver driver) {
		super(driver);

	}

	public void login(String Usuario, String Password) {
		txtUsuario.sendKeys(Usuario);
		txtPassword.sendKeys(Password);
		btnIngreso.click();
			  
	} 
	
	public void validarSimular() {
		MatcherAssert.assertThat("no inicio sesion", simular.isDisplayed());
		simular.click();
	}

}

