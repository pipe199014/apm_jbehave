Narrative:
Yo como usuario de Sufi quiero 
simular un credito para la alianza AKT
donde se incluyan tramites para moto

Scenario: Simulacion_AKT_Tramites
Given que estoy en la aplicación de sufi
When inicio sesion con usuario y contraseña
Then verifico login correcto y doy clic en simular
When diligencio la informacion vendedor
When diligencio la informacion producto AKT
When diligencio la informacion de los campos Marca y Linea
Then doy clic en agregar tramites y accesorios
When diligencio los campos para registrar accesorios o tramites
When diligencio la informacion solicitante
Then doy click en el boton de simular
When verifico la informacion del credito solicitado
Then doy click en el boton radicar
When acepto las condiciones de la simulacion

Examples:
 
| Usuario |  Password   |PuntoVenta | Categoria | SubCategoria | Marca | Linea | ValorProd | Tipo_Doc   | Num_Doc    |Nombre_1    | Nombre_2    | Apellido_1    | Apellido_2   | Celular    | Correo             |Categoria_Acces_Tram|SubCategoria_Acces_Tram|valor_Acces_Tram|
| stmosco | Colombia2018|   109     |    142    |     451      | 2478  | 126490| 4000000   |   59       | 16590705   |OCTAVIO     |             | AROS          | SUAREZ       | 3004357890 | usuario@gmail.com  |146				  |463                     |800000         |

