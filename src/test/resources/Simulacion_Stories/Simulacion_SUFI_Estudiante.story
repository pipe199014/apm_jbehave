Narrative:
Yo como usuario de Sufi quiero 
simular un credito para la alianza SUFI

Scenario: Simulacion_SUFI
Given que estoy en la aplicación de sufi
When inicio sesion con usuario y contraseña
Then verifico login correcto y doy clic en simular
When diligencio la informacion vendedor
When diligencio la informacion producto SUFI
When diligencio la informacion solicitante y-o Estudiante
Then doy click en el boton de simular
When verifico la informacion del credito solicitado
Then doy click en el boton radicar
When acepto las condiciones de la simulacion

Examples:
| Usuario |  Password   |PuntoVenta | Categoria | SubCategoria |  ValorProd | Tipo_Doc   | Num_Doc    |Nombre_1    | Nombre_2    | Apellido_1    | Apellido_2   | Celular    | Correo             |Tipo_Doc_Estudiante   | Num_Doc_Estudiante|Nombre_1_Estudiante| Nombre_2_Estudiante| Apellido_1_Estudiante| Apellido_2_Estudiante| Celular_Estudiante| Correo_Estudiante     |estudianteEsDeudor|
| stmosco | Colombia2018|   75      |    150    |     476      |  2500000   |   59       | 16590705   |OCTAVIO     |             | AROS          | SUAREZ       | 3004357890 | usuario@gmail.com  | 59                   | 1094936648        | Steve             |                    |  Trujillo            | Monterrey            | 3016726353        | estudiante@gmail.com  |Si                |
