Narrative:
Yo como usuario de Sufi quiero 
simular un credito para la alianza AKT


Scenario: Simular_AKT
Given que estoy en la aplicación de sufi
When inicio sesion con usuario y contraseña
Then verifico login correcto y doy clic en simular

Examples:
 
| Usuario |  Password   | 
| stmosco | Colombia2018|  

Scenario: Simular_AKT2

When diligencio la informacion vendedor
When diligencio la informacion producto AKT
When diligencio la informacion de los campos Marca y Linea
When diligencio la informacion solicitante
Then doy click en el boton de simular
When verifico la informacion del credito solicitado
Then doy click en el boton radicar
When acepto las condiciones de la simulacion
When Valido que me encuentre el la pagina de actividad economica
Then selecciono la actividad economica y doy clik en continual
 
 

Examples:
 
|PuntoVenta | Categoria | SubCategoria | Marca | Linea | ValorProd | Tipo_Doc   | Num_Doc    |Nombre_1    | Nombre_2    | Apellido_1    | Apellido_2   | Celular    | Correo            | ocupacion |
|   109     |    142    |     451      | 2478  | 126490| 4000000   |   59       | 16590705   |OCTAVIO     |             | AROS          | SUAREZ       | 3004357890 | usuario@gmail.com | 122       |

 